\documentclass[
    12pt, % tamanho da fonte
    a4paper, % tamanho do papel.
    oneside, % impede a configuração para impressão frente e verso
    english, % idioma adicional para hifenização
    brazil % o último idioma é o principal do documento
]{abntex2}

% ---
% Pacotes básicos
% ---
\usepackage{lmodern}
\usepackage[T1]{fontenc} % Selecao de codigos de fonte.
\usepackage[utf8]{inputenc} % Codificacao do documento (conversão automática dos acentos)
\usepackage{csquotes}
\usepackage{indentfirst} % Indenta o primeiro parágrafo de cada seção.
\usepackage{color} % Controle das cores
\usepackage{graphicx} % Inclusão de gráficos
\usepackage{microtype} % para melhorias de justificação
\usepackage{csquotes} % para estilizar a citação
% ---

% Símbolo ordinal
\usepackage{newunicodechar}
\newunicodechar{°}{\ensuremath{^\circ}}

% ---
% Pacotes de citações
% ---
\usepackage[style=abnt]{biblatex}
\addbibresource{referencias.bib}

% ---
% Informações de dados para CAPA e FOLHA DE ROSTO
% ---
\title{Vantagens do software proprietário e desvantagens do software livre \\ \small{Computação e Sociedade}}
\author{Bruno Mello\and Geraldo Fada\and Luciano Lage\and Rodrigo Correa}
\local{Brasil, Rio de Janeiro}
\date{10 de Agosto 2021\\ \small{1° Semestre}}
\orientador[Professor:]{José Raphael Bokehi}
\instituicao{Universidade Federal Fluminense}
\tipotrabalho{Trabalho Acadêmico}

% ---
% Configurações de aparência do PDF final
% ---
% alterando o aspecto da cor azul
\definecolor{blue}{RGB}{60,100,120}
% informações do PDF
\makeatletter
\hypersetup{
    %pagebackref=true,
    pdftitle={\@title},
    pdfauthor={\@author},
    pdfsubject={\imprimirpreambulo},
    pdfcreator={LaTeX with abnTeX2},
    pdfkeywords={abnt}{latex}{abntex}{abntex2}{trabalho acadêmico},
    colorlinks=true, % false: boxed links; true: colored links
    linkcolor=blue, % color of internal links
    citecolor=blue, % color of links to bibliography
    filecolor=magenta, % color of file links
    urlcolor=blue,
    bookmarksdepth=4
}
\makeatother
% ---

% ---
% Espaçamentos entre linhas e parágrafos
% ---
% O tamanho do parágrafo é dado por:
\setlength{\parindent}{1.3cm}
% Controle do espaçamento entre um parágrafo e outro:
\setlength{\parskip}{0.2cm}  % tente também \onelineskip

% ----
% Início do documento
% ----
\begin{document}

% Seleciona o idioma do documento (conforme pacotes do babel)
\selectlanguage{brazil}

% Retira espaço extra obsoleto entre as frases.
\frenchspacing

\imprimirfolhaderosto

% ---
% inserir o sumario
% ---
\pdfbookmark[0]{\contentsname}{toc}
\tableofcontents*
\cleardoublepage

\textual
\chapter{Introdução}
No cotidiano de toda a população, a tecnologia está cada vez mais inserida,
seja através de computadores, notebooks, smartphones, tablets, entre outros.
O uso dessas tecnologias normalmente se dá através de algo físico, algo
palpável, que denotamos de hardware, porém ele sozinho não faz muita coisa,
é somente uma junção de componentes eletrônicos que tem um objetivo específico.
Este objetivo é alcançado utilizando um conjunto de instruções que dita ao
hardware o que fazer, chamamos esse conjunto de instruções de software.

O software é basicamente um programa desenvolvido por um grupo ou por uma pessoa
para executar certa tarefa. Com o constante avanço da tecnologia, o software acabou
sendo ramificado. Seguindo a definição proposta pelo site RockContent \cite{TiposSoftwareQuais2019},
podemos classificar os softwares em três tipos principais: softwares de sistema,
softwares de aplicação e softwares de programação. Para entender a classificação de
softwares de sistema podemos pegar como exemplo os sistemas operacionais, como Android,
Windows, MacOS e as distribuições Linux como Ubuntu, Linux Mint, Pop!\_OS, Fedora, Arch Linux, entre outras.

Esta primeira camada de software possui como objetivo gerenciar os recursos físicos do
hardware e prover uma interface comum para as aplicações que nele rodam. Já a camada de
softwares de aplicação engloba todos os aplicativos que utilizamos no dia a dia, como
videogames, aplicações de reprodução de mídia, exploradores de arquivos, editores de texto,
editores de imagem e vídeo, entre outros. Por último temos a camada de softwares de
programação, que funciona como o meio termo entre as duas etapas descritas anteriormente.
Compiladores, drivers e bibliotecas de ligação dinâmica (arquivos .dll no windows e .so no linux)
são exemplos de software que entram na categoria de programas de aplicação.

Como a informática tem se tornado cada vez mais importante, tanto socialmente quanto
economicamente, esses programas tem alguns caminhos legais a seguir, às vezes acabam
virando propriedade intelectual de certa empresa ou pessoa, que são os chamados softwares
proprietários e às vezes acabam se tornando um projeto de domínio público, algo colaborativo
que diversas pessoas podem contribuir, o que chamamos de softwares livre.

Dito isso, podemos pensar que há diferenças entre software proprietário e livre, havendo
vantagens e desvantagens em cada um, e então cabe às pessoas da área da tecnologia
estudarem esses detalhes para, no momento de criação de um projeto, estarem cientes
dessas diferenças e escolher se o projeto deve ser com código fechado ou aberto. Para que
se possa escolher por qual caminho seguir, seja um software livre ou proprietário, é
necessário responder alguns questionamentos, como quem será o público alvo do projeto, que
espaço no mercado se tem como meta, qual a velocidade pretendida para avanço do projeto,
como será realizado o controle de versões, só depois de responder perguntas desse tipo
que se é possível tomar uma decisão final.


\chapter{Desenvolvimento}

\section{Software sem uma visão única não progride}
Existe um ponto negativo que afeta muito os softwares livres e é
algo inerente à sua concepção. Quando algo é feito de forma colaborativa,
por definição, não existe uma hierarquia propriamente dita, pois é a contribuição
da comunidade que constrói esse algo e é nesse ponto que o software livre é
abalado em relação aos seus competidores proprietários: a falta de hierarquia.

A ideia de que um software é livre e que qualquer pessoa pode contribuir com
sua construção se desemboca em dois caminhos, ou o conceito de liberdade é
quebrado e não é realmente todo mundo que pode contribuir ou de fato existe
uma tentativa de aceitar todo e qualquer tipo de contribuição. De toda forma,
ambas situações afetam igualmente os projetos abertos. Existe também um
terceiro ramo, provavelmente o mais comum e o que gera as piores
consequências, que ocorre quando o primeiro caminho é tomado, mas devido às
suas consequências e sua grandeza no problema esse caso será analisado mais à
frente neste artigo.

Para avaliar o primeiro caso podemos usar o repositório do kernel Linux como
exemplo, que é software aberto de talvez maior importância atualmente. Sendo
responsável por mais de 40\% do tráfego na internet \cite{LinuxVsWindows}, pelos serviços AWS
da Amazon que silenciosamente rodam mais de um terço dos sites da web e usam
Linux por trás \cite{WhatAWSDoes2020,brandomUsingInternetAmazon2018,clarkHowAmazonWeb2014}
e além disso, também está por trás do Android que por
si só domina o mercado de celulares com mais 70\% \cite{galov101AndroidStats2019,burnettePatrickBradyDissects2008},
fica fácil notar a importância desse projeto para o mundo. O kernel Linux é
famoso por ter uma única pessoa que encabeça todo o projeto (Linus Torvalds),
aceitando apenas o que ele julga ser de excelência dos seus colaboradores
\cite{swannerLinuxCreatorLinus2015,osborneLinusTorvaldsDon2017}, esse simples fato
já enfatiza o ponto: o kernel Linux não é de fato aberto, visto que não é qualquer
um que pode modificá-lo.

Olhando por outra lado, podemos atribuir grande parte do sucesso do kernel Linux
a essa hierarquia extremamente bem definida e consequentemente à única visão que
o software segue, a do seu criador. Dessa forma temos como exemplo o maior ponto
fora da curva, que exatamente por não seguir a filosofia aberta, conseguiu seguir
uma única visão e manter seu nível de qualidade e o seu foco no que se propôs a fazer.

O segundo caso beira o impossível de achar exemplos, pois como já foi discutido acima,
um projeto que bota em prática de verdade o conceito de aberto teria de aceitar
contribuições de qualquer pessoa e isso torna qualquer tipo de progressão
inviável. Basta pensar do ponto de vista de um engenheiro, que precisa construir
uma ponte, mas ao mesmo tempo existem milhares de pessoas trocando o tipo
de material que vai ser utilizado, redesenhando o projeto, mudando o destino
final da ponte, enfim, é simplesmente inviável. E o próprio fato de não existir
nenhum caso bem conhecido aplicando ao pé da letra esse conceito já mostra que nenhum
projeto se sustentou de fato aplicando esse método.

Agora entrando no caso citado no segundo parágrafo que gera as piores consequências:
a fragmentação. Esse problema ocorre quando uma pessoa ou uma parte da comunidade
possui uma visão diferente de um projeto, e por não poder contribuir ou não concordar
com a versão vigente se cria uma nova. A partir dessa ideia surgiu o conceito do
fork, criado pelo github, em que se cria uma cópia do projeto.

O Android é um dos maiores exemplos desse problema, por ser um projeto também open
source \cite{PlatformSuperprojectAndroid} as fabricantes de celulares criam versões
próprias para seus aparelhos,
a Samsung, Xiaomi, Google, LG, Huawei, Oppo, Motorola, Sony, Asus, OnePlus são alguns
exemplos de fabricantes que suportam apenas suas variantes de android e as mesmas
partem de versões específicas do android original, gerando uma fragmentação na
bases usuários. A principal consequência disso é a ausência de atualizações do sistema.
Em 2015, 40\% das pessoas utilizavam uma versão antiga do sistema Android, enquanto
apenas 0,8\% utilizavam a versão mais recente \cite{GraficoMostraComo2015}.

Enquanto isso na concorrência, a Apple lida com esse problema de forma muito melhor.
Como o IOS é um sistema disponibilizado para pouquíssimos aparelhos, em comparação ao sistema
Android, a entrega de atualizações também se dá de forma bem frequente. No mesmo ano de 2015,
enquanto o Android possuia uma base de apenas 0,8\% com os celulare na versão mais recente,
a Apple em menos de 24 horas já tinha mais de 20\% da sua base de usuários com os
dispositivos rodando a versão mais recente do IOS \cite{carnetteAppleIOSPasses2015}.

Pode-se citar outro exemplo mais nichado, os compositores do desktop Linux.
Compositores são pedaços de tecnologia utilizados para fazer uma interface
entre o sistema de janelas do sistema operacional e a computação gráfica \cite{sharmaCompositorsLinux2020},
ou seja, é com ele que qualquer sistema operacional moderno ganha sua aparência:
janelas transparentes e com efeito de blur, animações, sombreados e o mais importante:
o V-Sync, responsável por solucionar o bug visual de tearing (a tela parece
ficar se dividindo de tempos em tempos).

Um projeto que tinha como objetivo fazer exatamente essa camada entre a API
gráfica do computador com as janelas do sistema chamado Picom \cite{yshuiPicom2021}, sofreu
muito com a falta de uma visão única para seu projeto. Membros da comunidade
resolveram adicionar funcionalidades que não estavam disponíveis no projeto
oficial, levando a criação de mais outras três grandes versões do mesmo
software \cite{PicomStandaloneXorg}. Uma versão adicionou a capacidade de ter bordas redondas nas
janelas e o efeito de blur \cite{ibhagwanIbhagwanPicom2021}, outra melhorou o efeito de blur utilizando
um algorítmo mais performático \cite{bussePicom2021} e por fim a terceira tenta juntar ambas
features mas não consegue se manter sempre atualizada \cite{burgaPicom2021} pois falta
contribuintes e os donos originais não abandonam suas visões do projeto,
o que faz com que eles se mantenham com suas próprias versões. 

Além de gerar inconsistências entre os projetos, os usuários que resolvem
utilizar esses compositores se prejudicam pois precisam decidir entre
funcionalidades que poderiam facilmente coexistir. Também é prejudicial para
as empresas por trás das distribuições do desktop Linux que precisam manter
essas funcionalidades e acabam por criar ainda mais uma versão nova para
o seu projeto \cite{kochtaLinuxDesktopCompositors2018}.

É possível perceber como a falta de uma visão única não beneficia nenhum
projeto na comunidade aberta e ainda sim esse é um problema que não tem
como fugir, pois o próprio conceito de software livre cria essas fragmentações.

\section{Não atende bem um mercado específico}
O mercado de tecnologia vem crescendo cada vez mais e move uma grande
parte da economia de alguns países. Partindo do pressuposto de que, no processo
de desenvolvimento de software, o lucro é visado, atender uma demanda de
nicho é algo muito mais lucrativo do que ter um projeto
mais abrangente. Quando esse projeto abrangente é aberto pra todo mundo
opinar, dificilmente consegue-se monetizar uma certa mudança e
ganhar dinheiro com isso. Além disso, como todos possuem acesso àquela
informação, dificilmente ela progride facilmente para fazer o que queremos
rapidamente.

Podemos olhar como indicativo disso o mercado empresarial, que contrata
softwares por demanda para realizar certas tarefas específicas ou até
mesmo gerenciar os recursos internos da firma. Qualquer um pode fazer
isso, desde um consultório ortodôntico até grandes conglomerados de
tecnologia da informação. Existem empresas que, assim como descritas na seguinte
matéria \cite{XAppsSoftwareSob2019}, podem ser consideradas ``fábricas de apps'', que
geralmente desenvolvem um programa para fazer um certo processo e que,
depois daquele esqueleto construído, fazem mudanças específicas para suprir
a necessidade da empresa contratante.

Se todos tem acesso ao esqueleto, ninguém pagaria por ele e sim
somente a mudança, se tornando muito menos rentável. A parte
mais difícil em si é construir aquela base, como ela já
possui uma estrutura inicial, pode haver até a
possibilidade da empresa que queria aquele tal software
construir de maneira própria uma versão que faz o que ela quer,
e sem o incentivo financeiro muitas vezes ninguém vai construir tal esqueleto.

Outro ponto que acaba influenciando é que quando há algum problema naquela
aplicação, os usuários tem vontade de encontrar um suporte técnico do produto,
algo que dificilmente ocorreria com o open source. Até pelo fato de que
muitas vezes a utilização é pouco intuitiva para o usuário, arrumar problemas
é ainda mais complicado. Muitas vezes as empresas contratantes até pagam para
ter o suporte da aplicação, tornando ainda mais lucrativo e movendo ainda
mais dinheiro. 

Pode-se ver isso até mesmo na comunidade open source, em que as empresas que mandam
são as gigantes, como a Red-Hat, que fornecem distribuições linux de forma
empresarial e cobrando. A Canonical também começou a seguir por esse lado
com versões específicas do Ubuntu, e isso só funciona pois além de prover
o sistema operacional a empresa presta serviço de suporte. Essa ideia acaba fugindo
do conceito de código aberto pois essas versões são proprietárias. 

\section{Pouco intuitivo para o usuário final}
Uma das características que desfavorece a utilização de softwares open source
é que muitos projetos abertos só estão disponíveis para uma pequena parcela
de pessoas que tenha o conhecimento técnico necessário para utilizar o programa,
existem casos em que o usuário precisa compilar o programa diretamente da fonte. 

Como exemplo para o caso, o integrante do grupo Bruno Mello utiliza o software
livre LogiOps, que é utilizado em sistemas operacionais Linux. A aplicação
tem como objetivo mapear funções dos mouses da LogiTech, pois a empresa apenas
desenvolveu uma solução para Windows, deixando usuários Linux sem
suporte para utilizar seus mouses. Para contornar esse problema a própria comunidade se uniu
para desenvolver o LogiOps \cite{garciaexplicaLogitechOptionsPara2021}.
O grande problema identificado por Bruno
foi o fato de ser preciso compilar o código fonte do software via prompt de comando, além
de não possuir uma interface gráfica para realizar ajustes do mouse.

Seguindo por essa linha de raciocínio, outro problema relevante é o fato de esses softwares
costumam ser pouco intuitivos para o usuário final, pois
nem sempre o usuário tem os conhecimentos necessários para resolver determinados
problemas que podem ocorrer, não tendo, necessariamente, fácil acesso
as respostas para esses problemas. 

Podemos utilizar como exemplo a Valve que está desenvolvendo uma funcionalidade
na Steam para executar jogos em Linux que não foram desenvolvidos propriamente
para esse tipo de sistema operacional. Realizando uma rápida busca pela internet,
podemos facilmente achar tutoriais extremamente longos e complexos como o
vídeo ``Como Jogar no Linux em 2020 - Guia Completo (+Dicas Avançadas)'' do Youtuber
Diolinux \cite{diolinuxComoJogarNo2020}. Existem diversos relatos de usuários que tiveram problemas com
essa funcionalidade e que para poder jogar os seus jogos, deveriam executar
scripts manuais via prompt de comando, que como já informado anteriormente,
não é todo mundo que possui esse conhecimento.

Parte desses problemas relacionados à utilização pelo usuário final se dá em função
da maioria dos softwares open source ser mantida quase que única e exclusivamente
por equipes pequenas ou pela comunidade. Dessa forma não é possível atender toda
a demanda que o mercado precisa, pois não existe mão de obra para testar tantas
possibilidades de sistemas operacionais e hardwares diferentes, o que acaba gerando
problemas de compatibilidade para diversas pessoas, que por usa vez gera mais demanda
para correção e consulta, que muitas vezes não tem resposta de imediato. Esse
problema se torna uma bola de neve, pois não é possível dispor de uma força
tarefa para atuar em tantos campos. Por último, sem visão econômica para o projeto em si é
mais difícil ainda investir para aumentar a equipe, que muitas vezes trabalha no ``amor''.

Assim como os problemas relacionados a compatibilidade, muitas vezes não existe
mão de obra o suficiente para atuar em uma interface gráfica intuitiva para o usuário,
e muitos programas rodam em sua totalidade via prompt de comando ou com pouca
interação gráfica. Tomando como exemplo o VLC Player, este utiliza uma interface
gráfica mais bruta e rústica, em tempos que o design da interface tem se tornado
cada vez mais simplificado, intuitivo e moderno. Navegando pelos fóruns da internet
é possível observar que diversos usuários do VLC Player no Linux apresentam
constantes problemas em sua interface gráfica, que muitas vezes só são resolvidos
recompilando o software. Como exemplo disso temos o tópico de um fórum direcionado
para o sistema operacional Ubuntu, em que um usuário reclama de um \textit{bug} em que
seu VLC inicia sem interface gráfica, precisando seguir passos via prompt de comando
para corrigir o problema \cite{VLCMediaPlayer2018}. 

Todos esses pontos criam um empecilho para usuários leigos que por ventura tentem
utilizar esses softwares. Dessa forma a evolução dos mesmos fica estagnada,
pois não ter público equivale a não ter sucesso no mercado e sem sucesso no
mercado não existe avanço do projeto.

\section{Dificuldade de empacotamento}
Outro ponto de desvantagem do software open source, como explica Linus Torvalds em
uma DebConf \cite{gentoomanLinusTorvaldsWhy2021}, dessa vez focando especificamente no empacotamento e
distribuição de programas, é a existência de diversas variações de sistemas operacionais
baseados em Linux, fazendo com que seja necessária a compilação e manutenção de
binários para cada uma dessas versões.

Um desenvolvedor de uma aplicação consegue facilmente criar um executável do seu programa para Windows,
e esse mesmo executável será utilizado por todos os usuários desse sistema, no máximo sendo necessária a
distinção entre um binário de 32 bits e outro de 64 bits. O mesmo pode ser dito
para aplicações geradas para MacOS: o desenvolvedor só precisa se preocupar em compilar
para esta plataforma. No caso de Linux isso é um pouco diferente, existem sistemas
baseados em Debian, Arch, BSD, entre outros, e até sistemas de mesma base possuem peculiaridades.
Ao compilar para Debian Stable, por exemplo, é necessário que o programa esteja de
acordo com certas regras de uso de bibliotecas compartilhadas para que esse programa
seja aceito nos repositórios da distribuição.

\begin{displayquote}
``Making binaries for linux desktop applications is a major fucking pain in the ass.''
- Linus Torvalds
\end{displayquote}


% ----------------------------------------------------------
% Finaliza a parte no bookmark do PDF
% para que se inicie o bookmark na raiz
% e adiciona espaço de parte no Sumário
% ----------------------------------------------------------
\phantompart
% ---
% Conclusão
% ---
\chapter{Conclusão}
Tendo em vista todos os pontos discutidos nesse documento, podemos
perceber que o software livre está longe de ser a prova de falhas.
No entanto, isso não significa que essas sejam as únicas características
a serem levadas em consideração. Podemos pegar como exemplo o Blender,
uma aplicação muito bem conceituada no mundo open source e amplamente
utilizada na criação de diversas produções comerciais de altíssima qualidade.

No fim, como qualquer outra dualidade na vida,
é importante colocar todos esses pontos na balança, pois
o melhor caminho a se seguir depende de fatores como: quem utilizará
o programa; como o projeto será introduzido no mercado; entre outros
fatores. Pois independente de ser livre ou proprietário,
haverão pontos positivos e pontos negativos, e escolher quais problemas
pretende-se enfrentar irá definir a taxa de sucesso que o projeto possa ter.

\postextual
% ----------------------------------------------------------
% Referências bibliográficas
% ----------------------------------------------------------
\printbibliography
%---------------------------------------------------------------------
% INDICE REMISSIVO
%---------------------------------------------------------------------
\phantompart
\printindex
%---------------------------------------------------------------------
\end{document}
